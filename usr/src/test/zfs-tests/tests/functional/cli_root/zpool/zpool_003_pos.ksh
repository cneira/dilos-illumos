#!/bin/ksh -p
#
# CDDL HEADER START
#
# The contents of this file are subject to the terms of the
# Common Development and Distribution License (the "License").
# You may not use this file except in compliance with the License.
#
# You can obtain a copy of the license at usr/src/OPENSOLARIS.LICENSE
# or http://www.opensolaris.org/os/licensing.
# See the License for the specific language governing permissions
# and limitations under the License.
#
# When distributing Covered Code, include this CDDL HEADER in each
# file and include the License file at usr/src/OPENSOLARIS.LICENSE.
# If applicable, add the following below this CDDL HEADER, with the
# fields enclosed by brackets "[]" replaced with your own identifying
# information: Portions Copyright [yyyy] [name of copyright owner]
#
# CDDL HEADER END
#

#
# Copyright 2007 Sun Microsystems, Inc.  All rights reserved.
# Use is subject to license terms.
#

#
# Copyright (c) 2012, 2016 by Delphix. All rights reserved.
#

. $STF_SUITE/include/libtest.shlib

#
# DESCRIPTION:
#	Verify debugging features of zpool such as ABORT and freeze/unfreeze
#	should run successfully.
#
# STRATEGY:
# 1. Create an array containing each zpool options.
# 2. For each element, execute the zpool command.
# 3. Verify it run successfully.
#

verify_runnable "both"

function cleanup
{

	coreadm_restore
}

log_assert "Debugging features of zpool should succeed."
log_onexit cleanup

CWD=$PWD

cd /tmp
log_must zpool -? > /dev/null 2>&1

if is_global_zone ; then
	log_must zpool freeze $TESTPOOL
else
	log_mustnot zpool freeze $TESTPOOL
	log_mustnot zpool freeze ${TESTPOOL%%/*}
fi

log_mustnot zpool freeze fakepool

# Remove corefile possibly left by previous failing run of this test.
[[ -f core ]] && log_must rm -f core

coreadm_save

# save OLDINIT
#OLDINIT=$(coreadm | grep "init core file pattern: " | sed -e 's/.*init core file pattern: //')
#log_must coreadm -i core
#OLDGLOBAL=$(coreadm | grep "global core dumps: " | sed -e 's/.*global core dumps: //')
#log_must coreadm -e global
#OLDPERPROCESS=$(coreadm | grep "per-process core dumps: " | sed -e 's/.*per-process core dumps: //')
#log_must coreadm -e process
# update for test
ZFS_ABORT=1; export ZFS_ABORT
zpool > /dev/null 2>&1
unset ZFS_ABORT
#log_must coreadm -i $OLDINIT
#if [[ "x$OLDGLOBAL" -eq "xdisabled" ]]; then
#	log_must coreadm -d global
#fi
#if [[ "x$OLDPERPROCESS" -eq "xdisabled" ]]; then
#	log_must coreadm -d process
#fi

[[ -f core ]] || log_fail "zpool did not dump core by request."
[[ -f core ]] && log_must rm -f core
cd $CWD

log_pass "Debugging features of zpool succeed."
