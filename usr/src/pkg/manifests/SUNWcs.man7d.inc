#
# This file and its contents are supplied under the terms of the
# Common Development and Distribution License ("CDDL"), version 1.0.
# You may only use this file in accordance with the terms of version
# 1.0 of the CDDL.
#
# A full copy of the text of the CDDL should have accompanied this
# source.  A copy of the CDDL is also available via the Internet
# at http://www.illumos.org/license/CDDL.
#

# Copyright 2011, Richard Lowe

dir path=usr/share/man/man7d
$(i386_ONLY)file path=usr/share/man/man7d/amdf17nbdf.7d
$(i386_ONLY)file path=usr/share/man/man7d/asy.7d
$(i386_ONLY)file path=usr/share/man/man7d/cmdk.7d
$(i386_ONLY)file path=usr/share/man/man7d/coretemp.7d
$(i386_ONLY)file path=usr/share/man/man7d/ecpp.7d
file path=usr/share/man/man7d/fd.7d
$(i386_ONLY)file path=usr/share/man/man7d/imc.7d
$(i386_ONLY)file path=usr/share/man/man7d/imcstub.7d
file path=usr/share/man/man7d/kstat.7d
file path=usr/share/man/man7d/ksyms.7d
$(i386_ONLY)file path=usr/share/man/man7d/pchtemp.7d
file path=usr/share/man/man7d/ptm.7d
file path=usr/share/man/man7d/pts.7d
