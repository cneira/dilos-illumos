PROG= priocntl
SRCS= subr.c
FSS_SRCS= fss$(PROG).c

CC= gcc
CFLAGS+= -ggdb3 -O0 -I../../lib/libcontract/common
LDADD+= -lcontract

OBJS= $(SRCS:.c=.o)
FSS_OBJS= $(OBJS) $(FSS_SRCS:.c=.o)

all: fss$(PROG)

fss$(PROG): $(FSS_OBJS)
	$(CC) -o $@ $^ $(LDADD)

clean:
	rm -f *.o fss$(PROG)
