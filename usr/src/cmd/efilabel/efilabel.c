/*
 * This file and its contents are supplied under the terms of the
 * Common Development and Distribution License ("CDDL"), version 1.0.
 * You may only use this file in accordance with the terms of version
 * 1.0 of the CDDL.
 *
 *  A full copy of the text of the CDDL should have accompanied this
 * source.  A copy of the CDDL is also available via the Internet at
 * http://www.illumos.org/license/CDDL.
 */

/*
 * Copyright 2016 Igor Kozhukhov <ikozhukhov@gmail.com>
 */

/*
 * efilabel - enable EFI label on disk
 */

#include <stdio.h>
#include <unistd.h>
#include <stdlib.h>
#include <string.h>
#include <fcntl.h>
#include <locale.h>
#include <libzfs.h>
#include <errno.h>
#include <limits.h>

#include <sys/types.h>
#include <sys/stat.h>
#include <sys/vtoc.h>

#define	DISK_ROOT	"/dev/dsk"
#define	RDISK_ROOT	"/dev/rdsk"
#define	BACKUP_SLICE	"s2"

/*
 * Definitions for Label types: L_TYPE_SOLORIS is the default Sun label
 * a.k.a VTOC. L_TYPE_EFI is the EFI label type.
 */
#define	L_TYPE_SOLARIS	0
#define	L_TYPE_EFI	1

/*
 * warn(): Print an error message. Always returns -1.
 */
static int
warn(char *what, char *why)
{
    (void) fprintf(stderr, "efilabel: what:%s, why:%s\n", what, why);
	return (-1);
}

static int
is_efi_type(char *devname)
{
	struct extvtoc vtoc;
	struct stat sb;
	int fd;
	char path[MAXPATHLEN];

	(void) snprintf(path, sizeof (path), "%s/%s%s", RDISK_ROOT, devname,
	        BACKUP_SLICE);

	if ((fd = open(path, O_NONBLOCK|O_RDONLY)) < 0)
		return (warn(devname, strerror(errno)));

	if (fstat(fd, &sb) < 0)
		return (warn(devname, strerror(errno)));

	if ((sb.st_mode & S_IFMT) != S_IFCHR)
		return (warn(devname, "Not a raw device"));

	if (read_extvtoc(fd, &vtoc) == VT_ENOTSUP) {
	/* assume the disk has EFI label */
		close(fd);
		return (1);
	}

	close(fd);
	return (0);
}

int main(int argc, char *argv[])
{
	int arg;
	int opt_d = 0;
	int do_efi = 0;
	char *name = NULL;
	libzfs_handle_t *g_zfs = NULL;
	zpool_boot_label_t boot_type = ZPOOL_NO_BOOT_LABEL;
	uint64_t boot_size = 0;

	(void) setlocale(LC_ALL, "");
	(void) textdomain(TEXT_DOMAIN);

	while ((arg = getopt(argc, argv, "d:f")) != -1) {
		switch (arg) {
		case 'd':
			opt_d = 1;
			name = argv[optind - 1];
			break;
		case 'f':
			do_efi = 1;
			break;
		default:
			(void) fprintf(stderr,
			    gettext("Usage: efilabel -d vdev [-f]\n"));
			return (1);
		}
	}

	if (argc < 2) {
		fprintf(stderr, "Error: Usage: efilabel -d vdev [-f]\n");
		return (1);
	}

	if (is_efi_type(name)) {
		printf("%s: already EFI\n", name);
		do_efi = 0;
	}

	if (opt_d && is_efi_type(name) != L_TYPE_EFI && do_efi == 0) {
		printf("%s: You have VTOC or another label.\n"
		"Please use -f to force EFI label.\n", name);
	}

	if (do_efi) {

		if ((g_zfs = libzfs_init()) == NULL) {
			(void) fprintf(stderr, gettext("internal error: failed"
			    " to initialize ZFS library\n"));
			return (1);
		}

		if (zpool_label_disk(g_zfs, NULL, name, boot_type,
		    boot_size, NULL) != 0)
		{
			(void) fprintf(stderr, gettext("Error: can't setup EFI"
			    " label to '%s'!\n"), name);
			return (1);
		}
		printf("Success EFI label to '%s'\n", name);
	}

	return (0);
}
