PROG= smtp-notify

VPATH=common
SRCS= smtp-notify.c
OBJS= $(SRCS:.c=.o)
CC= gcc
CFLAGS=-O0 -ggdb3 -nodefaultlibs -D__sun -D__dilos__ -D__dilos \
-m64 -Ui386 -U__i386 -Wall -Wextra \
-std=gnu99 -DTEXT_DOMAIN="SUNW_OST_OSCMD" -D_TS_ERRNO \
-I/export/home/denis/projects/dilos/illumos/proto/root_i386/usr/include \
-DDEBUG -Icommon -I../../../../lib/fm/libfmnotify/common
LDFLAGS=-nodefaultlibs -m64 -R/usr/lib/fm/amd64 \
-L/export/home/denis/projects/dilos/illumos/proto/root_i386/lib/amd64 \
-L/export/home/denis/projects/dilos/illumos/proto/root_i386/usr/lib/amd64 \
-L/export/home/denis/projects/dilos/illumos/proto/root_i386/usr/lib/fm/amd64 \
-lnvpair -lfmevent -lfmd_msg -lfmnotify -lumem -lc

all: $(PROG)

$(PROG): $(OBJS)
	$(CC) -o $@ $^ $(LDFLAGS)

clean:
	rm -f $(OBJS)

distclean: clean
	rm -f $(PROG)
