PROG= zdb

SRCTREE=../../../..
CC= gcc
CFLAGS+= -m64 -O0 -ggdb3 -nodefaultlibs -D__sun -D__dilos__ -D__dilos \
-Ui386 -U__i386 -std=gnu99 -DTEXT_DOMAIN=\"SUNW_OST_OSCMD\" -D_TS_ERRNO \
-I$(SRCTREE)/proto/root_i386/usr/include \
-D_LARGEFILE64_SOURCE=1 -D_REENTRANT \
-I../../lib/libzpool/common \
-I../../uts/common/fs/zfs \
-I../../common/zfs -DDEBUG

LDFLAGS+= -m64 -L$(SRCTREE)/proto/root_i386/usr/lib/64 \
-L$(SRCTREE)/proto/root_i386/lib/64 \
-lzutil -lzpool -lumem -lnvpair -lzfs -lavl -lcmdutils
# -lfakekernel

SRCS= zdb_il.c zdb.c
OBJS= $(SRCS:.c=.o)

all: $(PROG)

$(PROG): $(OBJS)
#	ln -sf /lib/64/libcmdutils.so.1 libcmdutils.so
	$(CC) -o $@ $^ $(LDFLAGS)
#	rm -f libcmdutils.so

clean:
	rm -f $(PROG) $(OBJS)
