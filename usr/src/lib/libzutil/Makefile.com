#
# This file and its contents are supplied under the terms of the
# Common Development and Distribution License ("CDDL"), version 1.0.
# You may only use this file in accordance with the terms of version
# 1.0 of the CDDL.
#
# A full copy of the text of the CDDL should have accompanied this
# source.  A copy of the CDDL is also available via the Internet at
# http://www.illumos.org/license/CDDL.
#

#
# Copyright 2019 DilOS Team
#

LIBRARY= libzutil.a
VERS= .1

OBJS_COMMON=			\
	gethostid.o		\
	zutil_device_path.o	\
	zutil_import.o		\
	zutil_nicenum.o		\
	zutil_pool.o		\
	zutil_syslog.o

OBJECTS= $(OBJS_COMMON)

include ../../Makefile.lib

# libzfs must be installed in the root filesystem for mount(1M)
include ../../Makefile.rootfs

LIBS=	$(DYNLIB)

SRCDIR =	../common

INCS += -I$(SRCDIR)
INCS += -I$(SRC)/uts/common/fs/zfs
INCS += -I../../../common/zfs
INCS += -I../../libc/inc

CSTD=	$(CSTD_GNU99)
C99LMODE=	-Xc99=%all
LDLIBS +=	-lnvpair -lavl -lefi -lblkid
LDLIBS +=	-ladm
LDLIBS +=	-lm -lc
CPPFLAGS +=	$(INCS) -D_LARGEFILE64_SOURCE=1 -D_REENTRANT
$(NOT_RELEASE_BUILD)CPPFLAGS += -DDEBUG

# not linted
SMATCH=off

SRCS=	$(OBJS_COMMON:%.o=$(SRCDIR)/%.c)

.KEEP_STATE:

all: $(LIBS)

pics/%.o: ../../../common/zfs/%.c
	$(COMPILE.c) -o $@ $<
	$(POST_PROCESS_O)

include ../../Makefile.targ
