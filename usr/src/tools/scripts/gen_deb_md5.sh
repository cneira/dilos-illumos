#!/bin/sh

# set -x
# VERSION 1.1

MD5SUM="$1"
TMPDIR="/var/tmp"
MD5NEW="md5sum.new.$$"

if [ -z "$MD5SUM" ]; then
    FNAME=`echo $MD5SUM | cut -f1 -d'.'`
    FNAME=${FNAME}".control"
    MD5SUM="$FNAME"
fi

MD5=""

if [ `cat $MD5SUM | grep -c "\.jar\$"` ]; then
    `cat $MD5SUM | sed -e '/\.jar$/d' > $TMPDIR/$MD5NEW` 
    MD5=`digest -a md5 $TMPDIR/$MD5NEW`
    rm -f $TMPDIR/$MD5NEW
elif [ `cat $MD5SUM | grep -c "\.ln\$"` ]; then
    `cat $MD5SUM | sed -e '/\.ln$/d' > $TMPDIR/$MD5NEW`
    MD5=`digest -a md5 $TMPDIR/$MD5NEW`
    rm -f $TMPDIR/$MD5NEW
else
    MD5=`digest -a md5 $TMPDIR/$MD5SUM`
fi
echo "$MD5"
