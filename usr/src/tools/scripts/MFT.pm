#
# This file and its contents are supplied under the terms of the
# Common Development and Distribution License ("CDDL"), version 1.0.
# You may only use this file in accordance with the terms of version
# 1.0 of the CDDL.
#
# A full copy of the text of the CDDL should have accompanied this
# source.  A copy of the CDDL is also available via the Internet at
# http://www.illumos.org/license/CDDL.
#

#
# Copyright (c) 2010-2013, Igor Kozhukhov <ikozhukhov@gmail.com>. All rights reserved.
#

#
# VERSION: 1.20
#

package MFT;

use FindBin;
use lib "$FindBin::Bin/";

use File::Path;
use File::Basename;

use strict;

#------------------------------
# Constructor
#------------------------------
sub new
{
    my $class = shift;
    my $self = {
        _fileName => '',
        _moduleName => '',
        _arch => '',
        _version => '5.11-0.200',
        _manifest => {},
        _scripts => {},
        _installproto => [],
        _depends => [],
        _specDefaults => [],
        _ignoreDepends => [],
        _ignoreDev => [],
        _isips => 0,
        _isdev => 0,
        _facets => {},
    };

    bless $self, $class;
    return $self;
}


###############################
#------------------------------
# function init()
#------------------------------
sub init
{
    my ($self) = @_;
    undef ($self->{_manifest});
#    $self->{_manifest} = {};
    undef ($self->{_scripts});
#    $self->{_scripts} = {};
    undef ($self->{_installproto});
#    $self->{_installproto} = [];
    undef ($self->{_depends});
    undef ($self->{_specDefaults});
    undef ($self->{_ignoreDepends});
    undef ($self->{_ignoreDev});
    undef ($self->{_facets});
    $self->{_isips} = 0;
    $self->{_isdev} = 0;
}

#------------------------------
# function getHostArch()
#------------------------------
sub getHostArch()
{
    my ($self) = @_;
    return $self->{_arch};
}

#------------------------------
# function setHostArch()
#------------------------------
sub setHostArch()
{
    my ($self, $arch) = @_;
    $self->{_arch} = $arch if defined($arch);
    return $self->{_arch};
}

#------------------------------
# function setIps()
#------------------------------
sub setIps()
{
    my ($self, $ips) = @_;
    $self->{_isips} = $ips;
    return $self->{_isips};
}

#------------------------------
# function setDev()
#------------------------------
sub setDev()
{
    my ($self, $dev) = @_;
    $self->{_isdev} = $dev;
    return $self->{_isdev};
}

#------------------------------
# function getScripts()
#------------------------------
sub getScripts
{
    my ($self) = @_;
    return $self->{_scripts};
}

#------------------------------
# function setVersion()
#------------------------------
sub setVersion
{
    my ( $self, $version ) = @_;
    $self->{_version} = $version if defined($version);
    return $self->{_version};
}

#------------------------------
# function getVersion()
#------------------------------
sub getVersion
{
    my ( $self ) = @_;
    return $self->{_version};
}

#------------------------------
# function getModuleName()
#------------------------------
sub getModuleName
{
    my ( $self ) = @_;
    return $self->{_moduleName};
}

#------------------------------
# function getFileName()
#------------------------------
sub getFileName
{
    my ( $self ) = @_;
    return $self->{_fileName};
}

#------------------------------
# function readFile()
#------------------------------
sub readFile
{
    my ( $self, $filename ) = @_;
#    my ( $filename ) = @_;

#    print $self->getFunctionName().", $filename\n";
#    my( $filename ) = shift;
    my $lines = [];
    my $templines = [];
    my $line = '';
    my $incfile = '';
    my $dirName = '';

    local *FILE;
    open( FILE, "<", $filename ) or die "Can't open '$filename' : $!";
    while( <FILE> ) {

        chomp;              # remove trailing newline characters

#        s/^\$\(sparc_ONLY\)/REMOVED /;
#        s/^\$\(i386_ONLY\)//;
#        s/\$\(ARCH64\)/amd64/g;
#        s/\$\(MACH64\)/amd64/g;
#        s/\$\(ARCH32\)/i86/g;
#        s/\$\(MACH\)/i86/g;
#        s/\$\(ARCH\)/i386/g;
#        s/\$\(PKGVERS\)/$self->{_version}/g;

#        s/#.*//;            # ignore comments by erasing them
        next if /^(\s)*$/;  # skip blank lines
        next if /^#/;       # skip comments lines
        next if /^\s*#/;    # skip comments lines

        if (/<include/)
        {
            $incfile = $_;
            $incfile =~ s/<include //;
            $incfile =~ s/>//;

            if ($incfile eq 'global_zone_only_component')
            {
                push (@$lines, 'file path=_fakeGlobalZone_ variant.opensolaris.zone=global');
                next;
            }

            $dirName = dirname($filename);
            $templines = $self->readFile($dirName.'/'.$incfile);
            push (@$lines, @$templines);
            next;
        }
        if (/<transform/)
        {
            next;
        }

        if ($_ =~ /\\$/)
        {
            chop($_);
            $line .= $_;
            next;
        }
        else
        {
            s/^\s*//;           # remove first space(s)
            $line .= $_;
        }

        if ($line =~ /^REMOVED/)
        {
            $line = '';
            next;
        }
#        print "== $line\n";

        push @$lines, $line;    # push the data line onto the array
        $line = '';
    }
    close FILE;

#    $self->{_fileName} = $filename;
#    $self->{_moduleName} = lc(basename($filename, ".mf"));

    return $lines;  # reference
}

#------------------------------
# function getData()
#------------------------------
sub getData()
{
    my ( $self, $str ) = @_;

#print "input: $str\n";
    $_ = $str;
    my $res = {};

    my $actions = {};

    my $buff = [];
    my $action = '';
    $$res{'action'} = $buff;

    $action = $_ =~ m/^(\S*)/;
    $action = $1;

#print "== action: $action\n";
    push(@$buff, $action);

    my $origstr = $str;
    $_ = $origstr;
    s/^\S*//; # remove $action

    my $tmpRes = {};
    $tmpRes = $self->parser($_);
    $$tmpRes{'action'} = $$res{'action'} if defined($tmpRes);
#&print_mog_line($tmpRes);

    return $tmpRes;
}

#------------------------------
# function parser()
#------------------------------
sub parser ()
{
    my ( $self, $line ) = @_;

    my $flag = 1;
    my $flag_cnt = 0;

    my $facets = {};
    $facets = $self->{_facets} if defined($self->{_facets});

    my $res = {};
    my $others = [];
    my $wrd = '';
    my @words = split(' ', $line);
    foreach my $word (@words)
    {
	my $buff = [];
	if ($word =~ /"/) #"
	{
	    $flag_cnt++;
	    $flag_cnt = 0 if ($flag_cnt > 1);
	}

	if ($flag_cnt)
	{
	    $wrd .= ' '.$word;
	    $flag = 0;
	}
	else
	{
	    if ($flag < 1 && $flag_cnt < 1)
	    {
		$wrd .= ' '.$word;
	    }
	    else
	    {
		$wrd = $word;
	    }
	    $flag = 1 unless ($flag_cnt);
	}
	$wrd =~ s/^\s*//;
	if ($flag)
	{
	    if ($wrd =~ /=/)
	    {
		my @tokens = split(/=/, $wrd, 2);
		if ($self->{_isips})
		{
		    if ($tokens[0] =~ /facet\./)
		    {
			$$facets{$tokens[0]} = $self->getFacet($tokens[0]) unless defined($$facets{$tokens[0]});
			unless ($$facets{$tokens[0]} eq $tokens[1])
			{
			    $self->{_facets} = $facets if ($self->{_isips});
			    undef $res;
			    return $res;
			}
		    }
		}
#		$tokens[1] =~ s/"//g;
		defined($$res{$tokens[0]}) ? $buff = $$res{$tokens[0]} : $$res{$tokens[0]} = $buff;
		push(@$buff, $tokens[1]);
	    }
	    else
	    {
		push(@$others, $wrd) if (length($wrd) > 2);
	    }
	    $wrd = '';
	}
    }
    $$res{'others'} = $others if (scalar(@$others) > 0);
    $self->{_facets} = $facets if ($self->{_isips});

    return $res;
}

#------------------------------
# function getFacet()
#------------------------------
sub getFacet()
{
    my ( $self, $facet ) = @_;
    my $output = `pkg facet -H $facet`;
    chomp $output;
    my @buff = split(' ', $output);
    my $res = ($facet eq $buff[0]) ? lc($buff[1]) : 'none';
    return $res;
}

#------------------------------
# function print_mog_line()
#------------------------------
sub print_mog_line()
{
    my($myhash) = @_;

    foreach my $act (keys %$myhash)
    {
        print "$act=$$myhash{$act}[0]\n";
    }
}

#------------------------------
# function readMfFile()
#------------------------------
sub readMfFile()
{
    my ( $self, $mffile ) = @_;

    my $result = {};
    my $actions = [];

#    print "Loading file: $mffile\n";
    $self->{_fileName} = $mffile;
#    $self->{_moduleName} = lc(basename($mffile, ".mf"));

    my $lines = $self->readFile($mffile);
#print "== read file\n";

    if ($self->{_isdev})
    {
	my $mffile2 = './';
	$mffile2 .= $self->getModuleName();
	$mffile2 .= '-dev.adds';
	my $lines2 = $self->readFile($mffile2) if (-f $mffile2);
#	print "==DEBUG:$mffile2, @$lines2\n" if (-f $mffile2);
	push(@$lines, @$lines2) if (-f $mffile2);
	my $devDepend = "depend fmri=".$self->getModuleName()." type=require";
	push(@$lines, $devDepend);
    }

    my $mf;
    foreach my $line (sort @{$lines})
    {
#	print "== $line\n";
#        if($self->{_moduleName} eq 'runtime-perl-510-module-sun-solaris')
###        if($self->{_moduleName} =~ /-sun-solaris$/)
###        {
###            $line =~ s/PLAT/i86pc/g;
###            $line .= ' mode=0555' if ($line =~ /.*\.so/);
###            $line .= ' mode=0444' if ($line =~ /.*\.(pm|bs)/);
###        }
#print "4 - $line\n";
        $mf = $self->getData($line);
        next unless(defined($mf));
        $actions = $$result{$$mf{'action'}[0]};
        push(@$actions, $mf);
        $$result{$$mf{'action'}[0]} = $actions;
    }
    $self->{_manifest} = $result;
#    return $result;

    $self->{_moduleName} = $self->getPkgName();
#    print "== $self->{_moduleName}\n";

    return $self->{_manifest};
}

#------------------------------
# function getManifest()
#------------------------------
sub getManifest
{
    my ( $self ) = @_;
    return $self->{_manifest};
}

#------------------------------
# function getActions()
#------------------------------
sub getActions
{
    my ( $self, $actions, $field ) = @_;

    my $result = [];

    foreach my $action (@$actions)
    {
        push(@$result, $$action{$field}[0]) if defined($$action{$field});
    }

    return $result;
}

#------------------------------
# function getDescription()
#------------------------------
sub getDescription
{
    my ( $self ) = @_;

    my $mf = $self->getManifest();

    return unless defined($$mf{'set'});

    my $output = 'none';
    my $r = $$mf{'set'};

    foreach my $line (@$r)
    {
        $output = $$line{'value'}[0] if ($$line{'name'}[0] eq 'pkg.description');
        $output =~ s/"//g; #"
    }

    return $output;
}

#------------------------------
# function getShortDescription()
#------------------------------
sub getShortDescription
{
    my ( $self ) = @_;

    my $mf = $self->getManifest();
    return unless defined($$mf{'set'});

    my $output = 'none';
    my $r = $$mf{'set'};

    foreach my $line (@$r)
    {
        $output = $$line{'value'}[0] if ($$line{'name'}[0] eq 'pkg.summary');
        $output =~ s/"//g; #"
    }

    return $output;
}

#------------------------------
# function getArch()
#------------------------------
sub getArch
{
    my ( $self ) = @_;

    my $mf = $self->getManifest();
    return unless defined($$mf{'set'});

    my $output = 'none';
    my $r = $$mf{'set'};

    foreach my $line (@$r)
    {
	if ($$line{'name'}[0] eq 'variant.arch')
	{
	    my $arr = @$line{'value'};
	    if (scalar(@$arr) > 1)
	    {
		foreach my $val (@$arr)
		{
		    if ($val eq $self->{_arch})
		    {
			$output = $val;
			last;
		    }
		}
	    }
	    else
	    {
		$output = $$arr[0];
		$output =~ s/["']//g; #"
	    }
	}
    }

    return $output;
}

#------------------------------
# function getPkgObsolete()
#------------------------------
sub getPkgObsolete
{
    my ( $self ) = @_;

    my $mf = $self->getManifest();
    return unless defined($$mf{'set'});

    my $output = 'none';
    my $r = $$mf{'set'};

    foreach my $line (@$r)
    {
        $output = $$line{'value'}[0] if ($$line{'name'}[0] eq 'pkg.obsolete');
        $output =~ s/"//g; #"
    }

    return $output;
}

#------------------------------
# function getPkgRenamed()
#------------------------------
sub getPkgRenamed
{
    my ( $self ) = @_;

    my $mf = $self->getManifest();
    return unless defined($$mf{'set'});

    my $output = 'none';
    my $r = $$mf{'set'};

    foreach my $line (@$r)
    {
        $output = $$line{'value'}[0] if ($$line{'name'}[0] eq 'pkg.renamed');
        $output =~ s/"//g; #"
    }

    return $output;
}


#------------------------------
# function getOrigVersion()
#------------------------------
sub getOrigVersion
{
    my ( $self ) = @_;

    my $mf = $self->getManifest();
    return unless defined($$mf{'set'});

    my $output = 'none';
    my $r = $$mf{'set'};

    foreach my $line (@$r)
    {
	if ($$line{'name'}[0] eq 'pkg.fmri')
	{
	    $output = $$line{'value'}[0];
#print "== 1 - $output\n";
	    last;
	}
    }
    $output =~ s/\S*\@//;
    $output =~ s/,\S*//;
#print "== 2 - $output\n";

    return $output;
}


#------------------------------
# function getOrigName()
#------------------------------
sub getOrigName
{
    my ( $self ) = @_;

    my $mf = $self->getManifest();
    return unless defined($$mf{'set'});

    my $output = 'none';
    my $r = $$mf{'set'};

    foreach my $line (@$r)
    {
	if ($$line{'name'}[0] eq 'pkg.fmri')
	{
	    $output = $$line{'value'}[0];
#print "== 1 - $output\n";
	    last;
	}
    }
    $output =~ s/\@\S*//;
    $output =~ s/\S*\///;
    $output =~ s/_/-/g;
    $output = lc($output);

#print "== 2 - $output\n";

    return $output;
}

#------------------------------
# function getPkgName()
#------------------------------
sub getPkgName
{
    my ( $self ) = @_;
#    my ( $self , $mf) = @_;

    my $mf = $self->getManifest();
    return unless defined($$mf{'set'});

    my $output = 'none';
    my $r = $$mf{'set'};

    foreach my $line (@$r)
    {
	if ($$line{'name'}[0] eq 'pkg.fmri')
	{
	    $output = $$line{'value'}[0];
#print "== 1 - $output\n";
	    last;
	}
    }
#    $output =~ s/\@\S*//;
    $output =~ s/\@.*$//;

    $output =~ s/\/openindiana.org\///;
    $output =~ s/\/opensolaris.org\///;
    $output =~ s/\/oi-experimental\///;
    $output =~ s/\/oi-dev\///;
    $output =~ s/\/oi-il\///;
    $output =~ s/\/pkg5-nightly\///;
    $output =~ s/\/on-nightly\///;
    $output =~ s/\/install-nightly\///;
    $output =~ s/\/pkg5-localizable\///;
    $output =~ s/\/install-localizable\///;
    $output =~ s/\/dilos.org\///;
    $output =~ s/\/du.dilos.org\///;
    $output =~ s/\/du.dilos.org-localizable\///;
    $output =~ s/\/dg.dilos.org\///;
    $output =~ s/\/solaris\///;
    $output =~ s/pkg:\///;

    $output =~ s/_/-/g;
    $output =~ s/\//-/g;
    $output = lc($output);

#print "== 2 - $output\n";

    return $output;
}


#------------------------------
# function getDepend()
#------------------------------
# get dependences packages names
sub getDepend
{
    my ( $self ) = @_;
    my $mf = $self->getManifest();
#    print "== dep: ".$self->getFileName()."\n";

    return unless defined($$mf{'depend'});

    my $result = [];
    my $specDefaults = $self->{_specDefaults};
    my $ignoreDepends = $self->{_ignoreDepends};

    my $r = $$mf{'depend'};

#    my $fmri = $self->getActions($depends, 'fmri');

    my $pkgName;
    my $pkgVer = '';
    my $str = "";
    my $out = "";
    my @buff;

#    $self->toLog("depend of: ".join(",", @$fmri));

    foreach my $line (@$r) 
    {
	my $fmri = $$line{'fmri'}[0] if defined($$line{'fmri'});
	next unless defined($fmri);
	$pkgVer = '';
	$pkgVer = $fmri if ($fmri =~ /\@/);
        $pkgVer =~ s/.*\@// if ($pkgVer =~ /\@/);
	my $type = $$line{'type'}[0] if defined($$line{'type'});
	next unless defined($type);
	next if ($type eq 'exclude');
	next unless (($type eq 'require') || ($type eq 'conditional'));
	my $facetdev = 1 if defined($$line{'facet.devel'});
	next if ($facetdev && ($self->{_isdev} == 0));
#	next unless ($type eq 'require');
	my $predicate = $$line{'predicate'}[0] if defined($$line{'predicate'});
        $predicate =~ s/\//-/g if defined($$line{'predicate'});
        $predicate =~ s/_/-/g if defined($$line{'predicate'});
        $predicate = lc($predicate) if defined($$line{'predicate'});

	my $dependFile = $$line{'pkg.debug.depend.file'}[0] if defined($$line{'pkg.debug.depend.file'});
	my $dependPath = $$line{'pkg.debug.depend.path'}[0] if defined($$line{'pkg.debug.depend.path'});
	$dependFile = "$dependPath/$dependFile" if defined($$line{'pkg.debug.depend.path'});

#print "==DEBUG: $predicate\n" if defined($$line{'predicate'});
#print "==DEBUG: defaults:$$specDefaults{$predicate}\n" if defined($$line{'predicate'});

	if (($type eq 'conditional') && defined($predicate))
	{
	    next unless (defined($$specDefaults{$predicate}));
	}

	$pkgName = $fmri;

        if ($pkgName =~ m/__TBD$/i)
        {
    	    $str = "/usr/bin/dpkg -S /$dependFile";
    	    $out = `$str`;
    	    @buff = split(':', $out);
    	    $pkgName = $buff[0];
    	    next if ($pkgName eq 'dpkg-query');
        }

        $pkgName =~ s/pkg:\/\/openindiana.org\///;
        $pkgName =~ s/pkg:\/\/opensolaris.org\///;
        $pkgName =~ s/pkg:\/\/pkg5-nightly\///;
        $pkgName =~ s/pkg:\/\/on-nightly\///;
	$pkgName =~ s/pkg:\/\/install-nightly\///;
	$pkgName =~ s/pkg:\/\/pkg5-localizable\///;
	$pkgName =~ s/pkg:\/\/install-localizable\///;
	$pkgName =~ s/pkg:\/\/dilos.org\///;
	$pkgName =~ s/pkg:\/\/du.dilos.org\///;
	$pkgName =~ s/pkg:\/\/du.dilos.org-localizable\///;
	$pkgName =~ s/pkg:\/\/dg.dilos.org\///;
	$pkgName =~ s/pkg:\/\/solaris\///;
        $pkgName =~ s/pkg:\///;
        $pkgName =~ s/\@.*$//;
        $pkgName =~ s/\//-/g;
        $pkgName =~ s/_/-/g;

        if ($pkgName =~ /^SUNW/i)
        {
	    next unless ($pkgName =~ /^SUNWcsd$/i || $pkgName =~ /^SUNWcs$/i);
        }

        $pkgName = lc($pkgName);

        next if ($pkgName =~ m/consolidation/i);
        next if defined($$ignoreDepends{$pkgName});

	$pkgName .= "\@".$pkgVer if (length($pkgVer) > 6);
        push(@$result, $pkgName);
        $pkgVer = '';
    }
    push(@$result, $self->{_moduleName}."\@".$self->{_version}) if ($self->{_isdev});

    return $result;
}

1;
