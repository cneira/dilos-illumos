#!/bin/bash

export LC_ALL=C

MACH=`uname -p`

SUDO=${SUDO:-/usr/bin/sudo -E DEBIAN_FRONTEND=noninteractive}

${SUDO} apt-get clean
${SUDO} apt-get autoclean
# ADD/UPDATE packages
${SUDO} apt-get update

# remove old packages
${SUDO}  apt-get remove -y \
gcc-5 \
libpython2.7-minimal \
libdwarf-dev \
libxslt1.1 \
gettext \
libssl-dev \
libssl1.0-dev \
gettext \
flex \
bison \
illumos-make \
illumos-cpp \
libcpp \
libuuid-dev \
tcl8.6-dev \
libdemangle1sun-dev \
pkg-config \
libglib2.0-dev \
c++-sunpro \
libpam-dev \
heimdal-multidev \
gcc-44
(( $? > 0 )) && exit

yes y | ${SUDO}  apt-get install -f
(( $? > 0 )) && exit 1

# install build deps
yes y | ${SUDO}  apt-get install -y \
coreutils \
binutils \
build-essential \
ccache \
gawk \
dpkg-dev \
libfile-fcntllock-perl \
debhelper \
reprepro \
flex \
bison \
libexpat1-dev \
libtecla-dev \
libxml2-dev \
libxml2-utils \
libnspr4-dev \
libnss3-dev \
libsnmp-dev \
zlib1g-dev \
lib32z1-dev \
m4 \
gettext \
developer-dtrace \
git \
sharutils \
libwrap0-dev \
libiconv-dev \
sunmake \
libcpp \
text-locale \
ncurses-base \
ncurses-bin \
libncurses5-dev \
lib32ncurses5-dev \
libncursesw5-dev \
lib32ncursesw5-dev \
libtinfo-dev \
lib32tinfo-dev \
perl \
libsun-solaris-perl \
libxml-parser-perl \
ncompress \
time \
libsqlite0-dev \
libdwarf-dev \
system-zones \
python-dev \
libsasl2-dev \
libssl-dev \
diffutils \
mandoc \
uuid-dev \
libsff1 \
comerr-dev \
libpam0g-dev \
libldap2-dev \
libsqlite3-dev \
libkrb5-dev \
samba-dev \
libblkid-dev
(( $? > 0 )) && exit 1
#libpcidb1
#libima1
#libiscsit1
#libstmf1
#libdiskmgt1
#heimdal-multidev \

# workaround for pkg5
yes y | ${SUDO}  apt-get install -y \
python \
python-minimal \
python-openssl \
python-m2crypto \
python-pycurl \
python-jsonrpclib \
python-jsonschema \
python-cherrypy \
python-mako \
python-ply \
python-simplejson \
pkg5
(( $? > 0 )) && exit 1

# for libelf, libconv
#${SUDO}  apt-get install -y \
#c++-sunpro
#(( $? > 0 )) && exit

# for correct build dependencies:
${SUDO}  apt-get install -y \
libbrand \
libzonecfg \
libofmt \
lib32uuid1 \
libfstyp
(( $? > 0 )) && exit 1

# workaround for sparc
if [ "$MACH" = "sparc" ]; then
${SUDO}  apt-get install -y \
gcc-44 \
sun-as
(( $? > 0 )) && exit 1
fi

# workaround intel
if [ "$MACH" = "i386" ]; then
${SUDO}  apt-get install -y \
libppt1 \
libgrubmgmt1
(( $? > 0 )) && exit 1
fi

#sudo apt-get remove -y system-library-math-dev
#sudo apt-get remove -y libm-dev
yes y | ${SUDO} apt-get autoremove -y

####################
